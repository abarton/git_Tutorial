
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <thread>
#include <chrono>
using namespace std;
using namespace std::literals;
const bool debug = false;



bool doRoundfast(bool change) {
    //1 = goats
    //5 = car
    int x = rand() % 3;
    if(debug) cout << "car behind " << x << endl;
    //Player choses door
    int chosendoor = rand() % 3;
    if(debug) cout << "Player chooses "<< chosendoor << endl;
    //Monte Hall Opens door
    int doorsthatcanopen[2];
    int numofdoorstoopen=0;
    for(int i =0; i<3; i++) {
        if(i == chosendoor || i==x) continue;
        doorsthatcanopen[numofdoorstoopen] = i;
        numofdoorstoopen++;
    }
    int montechoice = rand() %  numofdoorstoopen;

    int monteopens  = doorsthatcanopen[montechoice];

    if(debug){ 
	   cout << "monte can open " << doorsthatcanopen[0];
       if(numofdoorstoopen == 2) cout << " or " << doorsthatcanopen[1] ;
       cout << " monte opens " << monteopens << endl;
    }

    //User decides to change

    if(change) {
        int changes = 0;
        int olddoor = chosendoor;
        if(debug) cout << "chosen door " << chosendoor  << "  monteopen " << monteopens << endl;
        for(int i=0; i<3; i++) {
//    cout << "chosen door " << chosendoor  << "  monteopen " << monteopens << endl;
            if(debug)       cout << "i " << i;
            if(i == chosendoor || monteopens==i) {
                if(debug)cout << " skip" << endl;
                continue;
            }
            chosendoor = i;
            if(debug)cout << " change " << endl;
            changes++;
            break;
        }

        if(debug)cout << "door changed from " << olddoor << " to " << chosendoor << endl;
        if(changes != 1) {
            cout << "Error " << endl;
            throw -6;
        }
    }

    bool win = chosendoor==x;
    if(debug) {
        if(win) cout << "win" << endl;
        else cout << "lost" << endl;
    }

    return win;
}



bool doRound(bool change) {
    //1 = goats
    //5 = car
    vector<int> doors(3, 1);
    int x = rand() % 3;
    if(debug) cout << "car behind " << x << endl;
    doors[x] = 5;
    //Player choses door
    int chosendoor = rand() % 3;
    if(debug) cout << "Player chooses "<< chosendoor << endl;
    //Monte Hall Opens door
    vector<int> doorsthatcanopen;
    doorsthatcanopen.reserve(2);
    for(int i =0; i<3; i++) {
        if(i == chosendoor || doors[i] == 5) continue;
        doorsthatcanopen.push_back(i);
    }

    if(doorsthatcanopen.size() > 2 || doorsthatcanopen.empty() ) cout << " error more than 2 doors" << endl;

    int monteopens  = doorsthatcanopen[ rand() %  doorsthatcanopen.size()] ;

    if(debug) cout << "monte can open " << doorsthatcanopen[0];
    if(doorsthatcanopen.size() == 2 && debug) cout << " or " << doorsthatcanopen[1] ;
    if(debug) cout << " monte opens " << monteopens << endl;
    if(doors[monteopens] == 5) cout << "error monte revealed car" << endl;

    //User decides to change

    if(change) {
        int changes = 0;
        int olddoor = chosendoor;
        if(debug) cout << "chosen door " << chosendoor  << "  monteopen " << monteopens << endl;
        for(int i=0; i<3; i++) {
//    cout << "chosen door " << chosendoor  << "  monteopen " << monteopens << endl;
            if(debug)       cout << "i " << i;
            if(i == chosendoor || monteopens==i) {
                if(debug)cout << " skip" << endl;
                continue;
            }
            chosendoor = i;
            if(debug)cout << " change " << endl;
            changes++;
            break;
        }

        if(debug)cout << "door changed from " << olddoor << " to " << chosendoor << endl;
        if(changes != 1) {
            cout << "Error " << endl;
            throw -6;
        }
    }

    bool win = doors[chosendoor] == 5;
    if(debug) {
        if(win) cout << "win" << endl;
        else    cout << "lost" << endl;
    }

    return win;
}


int main() {


    int changewins  =0;
    int keepwins    =0
    int numbofrounds = 10000;
    for(int i=0; i<numbofrounds; i++) {
        changewins += doRound(true);
    }
    for(int i=0; i<numbofrounds; i++) {
        keepwins += doRound(false);
    }

///***************
    std::this_thread::sleep_for(30s);
///***************
    float winratiochange = (float) changewins / numbofrounds;
    float winratiokeep = (float) keepwins / numbofrounds;
    cout << "Change wins " << changewins << endl;
    cout << "keep wins " << keepwins << endl;
    cout << "Win Ratios change " <<  winratiochange << endl;
    cout << "Win ratios keep " << winratiokeep << endl;

    return 0;
}

